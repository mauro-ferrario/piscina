#pragma once

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "ofxGui.h"
#include "ofxDeferredShading.h"
#include "Particles.h"

#define FULL_WIDTH 2560
#define FULL_HEIGHT 1440

using namespace ofxDeferred;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);

		ofEasyCam	cam;
		ofxAssimpModelLoader model;

		bool isShaderDirty;
		void setupDeferred();
		void updateDeferred();

		ofxDeferredProcessing deferred;
	/*	std::shared_ptr<PointLightPass> lightingPass;
		std::shared_ptr<SsaoPass> ssaoPass;
		std::shared_ptr<ShadowLightPass> shadowLightPass;
		*/
		std::shared_ptr<DofPass> dofPass;
		std::shared_ptr<SsaoPass> ssaoPass;
		std::shared_ptr<BloomPass> hdrPass;
		std::shared_ptr<UnderwaterPass> underwater;

		void setupGui();

		bool isShowPanel;
		ofxPanel panel;
		ofParameterGroup pl1;
		ofParameter<glm::vec3> pl1_pos;
		ofParameter<ofFloatColor> pl1_diff;
		ofParameter<ofFloatColor> pl1_spe;
		ofParameter<float> pl1_int;
		ofParameter<float> pl1_rad;

		ofParameterGroup pl2;
		ofParameter<glm::vec3> pl2_pos;
		ofParameter<ofFloatColor> pl2_diff;
		ofParameter<ofFloatColor> pl2_spe;
		ofParameter<float> pl2_int;
		ofParameter<float> pl2_rad;

		ofParameterGroup ao;
		ofParameter<float> ao_rad;
		ofParameter<float> ao_dark;

		ofParameterGroup shadow;
		ofParameter<ofFloatColor> sha_amb;
		ofParameter<ofFloatColor> sha_dif;
		ofParameter<float> sha_dark;

		ofParameterGroup dof;
		ofParameter<float> dof_blur;
		ofParameter<glm::vec2> dof_end_point;
		ofParameter<glm::vec2> dof_focul_range;
		
		ofMesh mesh1;
		ofMesh mesh2;
		ofMesh tMesh;

		ofTexture texture1;
		ofTexture texture2;

		ofMesh initMesh(ofxAssimpModelLoader& model);

		bool isDebug;
		vector<ofVec3f> posS;
		vector<float> rotS;

		Particles sys;
		float time;
		float timeStep;

		bool isUpdating;
};
