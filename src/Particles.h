#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "FboPingPong.h"


class Particles {

public:
	void init(int _texSize);
	void update(float _time, float _timeStep);
	void draw(ofCamera* _camera, bool isShadow);

	void updateParticles(float _time, float _timeStep);
	void drawParticles(shared_ptr<ofShader> _shader, ofCamera* _camera, bool isShadow);

	ofxPanel				gui;
	ofParameter<float>		particleMaxAge;
	ofParameter<float>		noisePositionScale;
	ofParameter<float>		noiseMagnitude;
	ofParameter<float>		noiseTimeScale;
	ofParameter<float>		noisePersistence;
	ofParameter<float>		twistNoiseTimeScale;
	ofParameter<float>		twistNoisePosScale;
	ofParameter<float>		twistMinAng;
	ofParameter<float>		twistMaxAng;

	ofParameter<ofVec3f>	baseSpeed;

	ofParameter<ofColor>	startColor;
	ofParameter<ofColor>	endColor;

	//ofParameter<ofColor>	materialDiffuse; // We will provide our own diffuse per particle
	ofParameter<ofColor>	materialAmbient;
	ofParameter<ofColor>	materialSpecular;
	ofParameter<ofColor>	materialEmissive;

	ofParameter<float>		materialShininess;

	shared_ptr<ofShader> particleUpdate;
	shared_ptr<ofShader> particleDraw;
	bool isShaderDirty;

	int						numParticles;
	int						textureSize;

	ofVboMesh				singleParticleMesh;

	


	FboPingPong				particleDataFbo;

	ofTexture tex;





	
};