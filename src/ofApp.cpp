#include "ofApp.h"



//--------------------------------------------------------------
void ofApp::setup(){
//	ofSetWindowPosition(-2560, 0);
	ofSetWindowPosition(0, 0);
	ofSetWindowShape(FULL_WIDTH, FULL_HEIGHT);
	cam.setNearClip(.1);
	
	sys.init(30);
	time = 0.0f;
	timeStep = 1.0f / 60.0f;
	
	//cam.setFarClip(8000);
	ofDisableNormalizedTexCoords();
	
	model.loadModel("megalodon.fbx");
	cout << "ANI: " << model.getAnimationCount() << endl;

	setupDeferred();
	setupGui();

	mesh1 = initMesh(model);
	texture1 = model.getTextureForMesh(0);
	model.loadModel("penguin.dae");
	mesh2 = initMesh(model);
	texture2 = model.getTextureForMesh(0);
	ofBoxPrimitive bb;
	bb.set(160);
//	test = bb.getMesh();
	for (int i = 0; i < 10; i++) {
		posS.push_back(ofVec3f(ofRandom(-100, 100), ofRandom(-20, 20), ofRandom(-20, 20)));
		rotS.push_back(ofRandom(-123, 213));
	}

	isUpdating = true;
	sys.singleParticleMesh.clear();
	sys.singleParticleMesh.append(mesh1);
	sys.singleParticleMesh.setMode(OF_PRIMITIVE_TRIANGLES);

	sys.tex = texture1;
}

ofMesh ofApp::initMesh(ofxAssimpModelLoader& model) {
	ofMesh mesh = model.getMesh(0);
	//for (int i = 0; i < mesh.getNumVertices(); i++) {
	//	ofVec2f texCoord;
	//	// <3 == ^_^
	//	texCoord.x = mesh.getTexCoord(i).x * model.getTextureForMesh(0).getWidth();
	//	texCoord.y = mesh.getTexCoord(i).y * model.getTextureForMesh(0).getHeight();
	//	mesh.setTexCoord(i, texCoord);
	//}
	ofMesh nuMesh;
	for (int i = 0; i < mesh.getNumIndices();i++) {
		nuMesh.addVertex(mesh.getVertex(mesh.getIndex(i)));
		nuMesh.addTexCoord(mesh.getTexCoord(mesh.getIndex(i)));
		nuMesh.addNormal(mesh.getNormal(mesh.getIndex(i)));
		nuMesh.addColor(ofColor(0));
	}
	return nuMesh;
}

//--------------------------------------------------------------
void ofApp::update(){

	updateDeferred();
	if (isUpdating) {
		if (ofGetKeyPressed(' ')) { timeStep = ofLerp(timeStep, ofMap(ofGetMouseX(), 0, ofGetWidth(), -(1.0f / 60.0f), (1.0f / 60.0f)), 0.1f); }
		else { timeStep = ofLerp(timeStep, 1.0f / 60.0f, 0.1f); }
		time += timeStep;

		sys.update(time, timeStep);
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	deferred.begin(cam,true);
	
	//deferred.getGBuffer().shader.setUniformTexture("meshTexture", texture1, 0);
	/*for (int i = 0; i < 10; i++) {
		ofPushMatrix();
		ofTranslate(posS[i]);
		ofScale(150);
		ofRotateY(rotS[i]);
		mesh1.draw(OF_MESH_FILL);
		ofPopMatrix();
	}*/
	//deferred.getGBuffer().shader.setUniformTexture("meshTexture", texture2, 0);
	//ofPushMatrix();
	//ofScale(150);
	////mesh2.draw(OF_MESH_FILL);
	//ofPopMatrix();
	ofPushMatrix();
	sys.draw(&cam, false);
	ofPopMatrix();

	
	deferred.end();

	if (isShowPanel) {
		//lightingPass->drawLights();
		//shadowLightPass->debugDraw();
		deferred.debugDraw();
		panel.draw();
	}

	//UnderwaterPass->chan0.draw(0, 0, 100, 100);
	//UnderwaterPass->chan1.draw(100, 0, 100, 100);
	//UnderwaterPass->chan2.draw(200, 0, 100, 100);
	//UnderwaterPass->chan3.draw(300, 0, 100, 100);
	if (isDebug) {
		underwater->guiDraw();
		sys.gui.draw();
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'f') {
		ofToggleFullscreen();
	}
	else if (key == '1') {
		isShowPanel = !isShowPanel;
	}
	else if (key == '2') {
		isDebug = !isDebug;
	}
	else if (key == '3') {
		posS.clear();
		rotS.clear();
		for (int i = 0; i < 10; i++) {
			posS.push_back(ofVec3f(ofRandom(-1000, 1000), ofRandom(-200, 200), ofRandom(-20, 20)));
			rotS.push_back(ofRandom(-123, 213));
		}
	}
	else if (key == ' ') {
		sys.isShaderDirty = true;
		//	underwater->isShaderDirty = !underwater->isShaderDirty;
	}
	cout << cam.getPosition() << endl;
}

//--------------------------------------------------------------
void ofApp::setupDeferred() {
	deferred.init(ofGetWidth(), ofGetHeight());
	
	//shadowLightPass = deferred.createPass<ShadowLightPass>();
	//shadowLightPass->setDarkness(0.9f);
	//shadowLightPass->setViewPortSize(1600.f);

	//lightingPass = deferred.createPass<PointLightPass>();
	//ofxDeferred::PointLight dlight;
	//dlight.ambientColor = ofFloatColor(0.005);
	//lightingPass->addLight(dlight);

	//dlight.ambientColor = ofFloatColor(0.0);
	//lightingPass->addLight(dlight);

	underwater = deferred.createPass<UnderwaterPass>();
	ssaoPass = deferred.createPass<SsaoPass>();
	hdrPass = deferred.createPass<BloomPass>();

	//dofPass = deferred.createPass<DofPass>();

}

void ofApp::updateDeferred() {
	/*lightingPass->getLightRef(0).position = pl1_pos.get();
	lightingPass->getLightRef(0).diffuseColor = pl1_diff.get();
	lightingPass->getLightRef(0).specularColor = pl1_spe.get();
	lightingPass->getLightRef(0).radius = pl1_rad.get();
	lightingPass->getLightRef(0).intensity = pl1_int.get();

	lightingPass->getLightRef(1).position = pl2_pos.get();
	lightingPass->getLightRef(1).diffuseColor = pl2_diff.get();
	lightingPass->getLightRef(1).specularColor = pl2_spe.get();
	lightingPass->getLightRef(1).intensity = pl2_int.get();
	lightingPass->getLightRef(1).radius = pl2_rad.get();*/

	ssaoPass->setOcculusionRadius(ao_rad.get());
	ssaoPass->setDarkness(ao_dark.get());

	/*shadowLightPass->setAmbientColor(sha_amb.get());
	shadowLightPass->setDiffuseColor(sha_dif.get());
	shadowLightPass->setDarkness(sha_dark.get());


	
;*/

	//dofPass->setEndPointsCoC(dof_end_point.get());
	//dofPass->setMaxBlur(dof_blur.get());
	//dofPass->setFoculRange(dof_focul_range.get());

}

//--------------------------------------------------------------
void ofApp::setupGui() {
	isShowPanel = false;

	panel.setup();
	//pl1.setName("Point light 1");
	//pl1.add(pl1_pos.set("Position", glm::vec3(500, 500, 500), glm::vec3(-1000), glm::vec3(1000)));
	//pl1.add(pl1_diff.set("Diffuse Color", ofFloatColor(0.4)));
	//pl1.add(pl1_spe.set("Specular Color", ofFloatColor(1.0)));
	//pl1.add(pl1_rad.set("Radius", 500, 100, 2000));
	//pl1.add(pl1_int.set("Intensity", 1.2, 0.1, 3.0));
	//panel.add(pl1);

	//pl2.setName("Point light 2");
	//pl2.add(pl2_pos.set("Position", glm::vec3(-600, 700, 200), glm::vec3(-1000), glm::vec3(1000)));
	//pl2.add(pl2_diff.set("Diffuse Color", ofFloatColor(0.4)));
	//pl2.add(pl2_spe.set("Specular Color", ofFloatColor(1.0)));
	//pl2.add(pl2_rad.set("Radius", 500, 100, 2000));
	//pl2.add(pl2_int.set("Intensity", 1.2, 0.1, 3.0));
	//panel.add(pl2);

	ao.setName("Ambient Occlusion");
	ao.add(ao_rad.set("Occlusion Radius", .05, 0.001, 1));
	ao.add(ao_dark.set("Darkness", 0.8, 0.1, 5.0));
	panel.add(ao);

	/*shadow.setName("Shadow Light");
	shadow.add(sha_amb.set("Ambient", ofFloatColor(0.2)));
	shadow.add(sha_dif.set("Diffuse", ofFloatColor(0.7)));
	shadow.add(sha_dark.set("Darkness", 0.5, 0., 1.));
	panel.add(shadow);*/

	/*dof.setName("Defocus Blur");
	dof.add(dof_blur.set("Max Blur", 0.5, 0.0, 10.0)); 
	dof.add(dof_end_point.set("setEndPointsCoC", glm::vec2(0.9,0.6), glm::vec2(0.0), glm::vec2(1.0)));
	dof.add(dof_focul_range.set("setFoculRange", glm::vec2(0.1,0.3), glm::vec2(0.0), glm::vec2(1.0)));
	panel.add(dof);*/
}
