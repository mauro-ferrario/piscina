#version 400
#extension GL_EXT_gpu_shader4 : require

//uniform sampler2D u_particleImageTexture;

uniform vec4 u_particleStartColor;
uniform vec4 u_particleEndColor;

// gbuffer
in vec4 vPosition;
in float vDepth;
in vec3 vNormal;
in vec2 vTexCoord;
in vec4 vColor;
in vec4 particlesPos;

uniform bool isShadow;

uniform sampler2DRect tex;

layout (location = 0) out vec4 outputColor0;
layout (location = 1) out vec4 outputColor1;
layout (location = 2) out vec4 outputColor2;
layout (location = 3) out vec4 outputColor3;
//////////////////////////////
out vec4 fragColor;

// --------------------------------------------
void main () {
	if(isShadow) {
		 outputColor0 =vec4(0,1,0,1);
  //       outputColor0.a = 1.;
        fragColor = vec4(0,1,0,1);
	}
	else {
		 outputColor0 = texture(tex,vTexCoord*vec2(4096,2048));
//		outputColor0 = texture(tex,vec2(vTexCoord.x, vTexCoord.y)*.2;
		//outputColor0 = texture(tex,vec2((vTexCoord.x + particlesPos.x) , (vTexCoord.y + particlesPos.y) ));
		outputColor1 = vPosition;
		outputColor2 = vec4(normalize(vNormal), vDepth);
	    outputColor3 = any(greaterThan(vColor, vec4(1.))) ? vColor : vec4(0.,0.,0.,1.);
	}
}