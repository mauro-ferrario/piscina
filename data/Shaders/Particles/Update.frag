#version 400

#extension GL_ARB_draw_buffers : enable

// Hmm, do we really need to give the path to the shader if it's in the same folder?
#define HALF_PI  1.57079632679489661923
#define PI       3.14159265358979323846
#define TWO_PI   6.28318530717958647693

float map( float value, float inputMin, float inputMax, float outputMin, float outputMax ) { return ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin); }
float mapClamped( float value, float inputMin, float inputMax, float outputMin, float outputMax ) { return clamp( ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin),    outputMin, outputMax ); }

vec3  map( vec3 value, vec3 inputMin, vec3 inputMax, vec3 outputMin, vec3 outputMax ) { return ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin); }

float stepInOut( float _edge1, float _edge2, float _val ) { return step(_edge1, _val) - step(_edge2,_val); }

float linearStep( float _edge0, float _edge1, float _t ) { return clamp( (_t - _edge0)/(_edge1 - _edge0), 0.0, 1.0); }
float linearStepInOut( float _low0, float _high0, float _high1, float _low1, float _t ) { return linearStep( _low0, _high0, _t ) * (1.0f - linearStep( _high1, _low1, _t )); }

float smoothStepInOut( float _low0, float _high0, float _high1, float _low1, float _t ) { return smoothstep( _low0, _high0, _t ) * (1.0 - smoothstep( _high1, _low1, _t )); }

float mod289(float x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec2 mod289(vec2 x)   { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec3 mod289(vec3 x)   { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec4 mod289(vec4 x)   { return x - floor(x * (1.0 / 289.0)) * 289.0; }

float rand(vec2 co) { return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453); }

float permute(float x) { return mod289(((x*34.0)+1.0)*x); }
vec3 permute(vec3 x)   { return mod289(((x*34.0)+1.0)*x); }
vec4 permute(vec4 x)   { return mod289(((x*34.0)+1.0)*x); }

float taylorInvSqrt(float r) { return 1.79284291400159 - 0.85373472095314 * r; }
vec4 taylorInvSqrt(vec4 r)   { return 1.79284291400159 - 0.85373472095314 * r; }


// ------------------------------------------------------------
mat4 makeLookAt(vec3 eye, vec3 center, vec3 up)
{
	mat4 M;
	
	vec3 zaxis = normalize(eye - center);
	vec3 xaxis = normalize( cross(up, zaxis) );
	vec3 yaxis = cross(zaxis,xaxis);
	
	M[0] = vec4(xaxis,0);
	M[1] = vec4(yaxis,0);
	M[2] = vec4(zaxis,0);
	M[3] = vec4(eye,1);
	
	return M;
}

// ------------------------------------------------------------
mat4 rotationMatrix(vec3 axis, float angle)
{
	axis = normalize(axis);
	float s = sin(angle);
	float c = cos(angle);
	float oc = 1.0 - c;
	
	return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
				oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
				oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
				0.0,                                0.0,                                0.0,                                1.0);
}


// --------------------------------------
vec3 randomPointOnSphere( vec3 _random )
{
	float lambda = _random.x;
	float u = map( _random.y, 0.0, 1.0, -1.0, 1.0 );
	float phi = _random.z * (2.0 * PI );
	
	vec3 p;
	p.x = pow(lambda, 1.0/3.0) * sqrt(1.0 - u * u) * cos(phi);
	p.y = pow(lambda, 1.0/3.0) * sqrt(1.0 - u * u) * sin(phi);
	p.z = pow(lambda, 1.0/3.0) * u;
	
	return p;
}


// ------------------------------------------------------------
float pulseSquare( float _t, float _frequency, float _width )
{
	return 1.0 - step( _width, mod( _t, _frequency ) );
}

// ------------------------------------------------------------
float pulseTriangle( float _t, float _frequency, float _width )
{
	float triangleT = mod( _t, _frequency ) / _width * 2.0;
	return (1.0 - abs(mod(triangleT,2.0) - 1.0)) * pulseSquare( _t, _frequency, _width );
}

// ------------------------------------------------------------
float pulseLineDownUp( float _t, float _frequency, float _width )
{
	float tmpVal = mod( _t, _frequency ) / _width;
	return tmpVal * (1.0 - step( 1.0, tmpVal ));
}

// ------------------------------------------------------------
float pulseLineUpDown( float _t, float _frequency, float _width )
{
	float tmpVal = 1 - (mod( _t, _frequency ) / _width);
	return clamp( tmpVal * (1 - step( 1.0, tmpVal )), 0, 1);
}

// ------------------------------------------------------------
float pulseSawTooth( float _t, float _frequency, float _width )
{
	float tmpVal = 1 - (mod( _t, _frequency ) / _width);
	return clamp( tmpVal * (1.0 - step( 1.0, tmpVal )), 0.0, 1.0);
}

// ------------------------------------------------------------
float pulseSine( float _t, float _frequency, float _width )
{
	float tmpVal = clamp( (mod( _t, _frequency ) / _width), 0.0, 1.0);
	return sin(tmpVal * PI);
}

// -----------------------------------------------------------
float pulseSmoothStep( float _t, float _frequency, float _x0, float _x1, float _x2, float _x3 )
{
	float tmpVal = mod( _t, _frequency );
	return smoothStepInOut( _x0, _x1, _x2, _x3, tmpVal ) ;
}

// -----------------------------------------------------------
float pulseLinearStep( float _t, float _frequency, float _x0, float _x1, float _x2, float _x3 )
{
	float tmpVal = mod( _t, _frequency );
	return linearStepInOut( _x0, _x1, _x2, _x3, tmpVal ) ;
}

// --------------------------------------
vec3 opTwistX( vec3 _p, float _angPerUnit )
{
	float c = cos( _angPerUnit * _p.x);
	float s = sin( _angPerUnit * _p.x);
	mat2  m = mat2(c,-s,s,c);
	return vec3( _p.x, m * _p.yz );
}

// --------------------------------------
vec3 opTwistY( vec3 _p, float _angPerUnit )
{
	float c = cos( _angPerUnit * _p.y);
	float s = sin( _angPerUnit * _p.y);
	mat2  m = mat2(c,-s,s,c);
	
	vec2 rotXZ = m * _p.xz;
	
	return vec3( rotXZ.x, _p.y, rotXZ.y );
}

// --------------------------------------
vec3 opTwistZ( vec3 _p, float _angPerUnit )
{
	float c = cos( _angPerUnit * _p.z);
	float s = sin( _angPerUnit * _p.z);
	mat2  m = mat2(c,-s,s,c);
	
	vec2 rotXY = m * _p.xy;
	
	return vec3( rotXY.x, rotXY.y, _p.z );
}

// --------------------------------------
float CatmullRom( float u, float x0, float x1, float x2, float x3 )
{
	float u2 = u * u;
	float u3 = u2 * u;
	return ((2 * x1) +
			(-x0 + x2) * u +
			(2*x0 - 5*x1 + 4*x2 - x3) * u2 +
			(-x0 + 3*x1 - 3*x2 + x3) * u3) * 0.5;
}

// http://www.iquilezles.org/www/articles/functions/functions.htm

// --------------------------------------
float cubicPulse( float c, float w, float x )
{
	x = abs(x - c);
	if( x>w ) return 0.0;
	x /= w;
	return 1.0 - x*x*(3.0-2.0*x);
}

// --------------------------------------
float expStep( float x, float k, float n )
{
	return exp( -k*pow(x,n) );
}

// --------------------------------------
float parabola( float x, float k )
{
	return pow( 4.0*x*(1.0-x), k );
}

// --------------------------------------
float pcurve( float x, float a, float b )
{
	float k = pow(a+b,a+b) / (pow(a,a)*pow(b,b));
	return k * pow( x, a ) * pow( 1.0-x, b );
}

// --------------------------------------
float impulse( float k, float x )
{
	float h = k*x;
	return h*exp(1.0-h);
}


vec4 grad4(float j, vec4 ip)
{
	const vec4 ones = vec4(1.0, 1.0, 1.0, -1.0);
	vec4 p,s;

	p.xyz = floor( fract (vec3(j) * ip.xyz) * 7.0) * ip.z - 1.0;
	p.w = 1.5 - dot(abs(p.xyz), ones.xyz);
	s = vec4(lessThan(p, vec4(0.0)));
	p.xyz = p.xyz + (s.xyz*2.0 - 1.0) * s.www; 

	return p;
}

#define F4 0.309016994374947451

vec4 simplexNoiseDerivatives (vec4 v)
{
	const vec4  C = vec4( 0.138196601125011,0.276393202250021,0.414589803375032,-0.447213595499958);

	vec4 i  = floor(v + dot(v, vec4(F4)) );
	vec4 x0 = v -   i + dot(i, C.xxxx);

	vec4 i0;
	vec3 isX = step( x0.yzw, x0.xxx );
	vec3 isYZ = step( x0.zww, x0.yyz );
	i0.x = isX.x + isX.y + isX.z;
	i0.yzw = 1.0 - isX;
	i0.y += isYZ.x + isYZ.y;
	i0.zw += 1.0 - isYZ.xy;
	i0.z += isYZ.z;
	i0.w += 1.0 - isYZ.z;

	vec4 i3 = clamp( i0, 0.0, 1.0 );
	vec4 i2 = clamp( i0-1.0, 0.0, 1.0 );
	vec4 i1 = clamp( i0-2.0, 0.0, 1.0 );

	vec4 x1 = x0 - i1 + C.xxxx;
	vec4 x2 = x0 - i2 + C.yyyy;
	vec4 x3 = x0 - i3 + C.zzzz;
	vec4 x4 = x0 + C.wwww;

	i = mod289(i); 
	float j0 = permute( permute( permute( permute(i.w) + i.z) + i.y) + i.x);
	vec4 j1 = permute( permute( permute( permute (   i.w + vec4(i1.w, i2.w, i3.w, 1.0 ))
												   + i.z + vec4(i1.z, i2.z, i3.z, 1.0 ))
												   + i.y + vec4(i1.y, i2.y, i3.y, 1.0 ))
												   + i.x + vec4(i1.x, i2.x, i3.x, 1.0 ));


	vec4 ip = vec4(1.0/294.0, 1.0/49.0, 1.0/7.0, 0.0) ;

	vec4 p0 = grad4(j0,   ip);
	vec4 p1 = grad4(j1.x, ip);
	vec4 p2 = grad4(j1.y, ip);
	vec4 p3 = grad4(j1.z, ip);
	vec4 p4 = grad4(j1.w, ip);

	vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
	p0 *= norm.x;
	p1 *= norm.y;
	p2 *= norm.z;
	p3 *= norm.w;
	p4 *= taylorInvSqrt(dot(p4,p4));

	vec3 values0 = vec3(dot(p0, x0), dot(p1, x1), dot(p2, x2)); //value of contributions from each corner at point
	vec2 values1 = vec2(dot(p3, x3), dot(p4, x4));

	vec3 m0 = max(0.5 - vec3(dot(x0,x0), dot(x1,x1), dot(x2,x2)), 0.0); //(0.5 - x^2) where x is the distance
	vec2 m1 = max(0.5 - vec2(dot(x3,x3), dot(x4,x4)), 0.0);

	vec3 temp0 = -6.0 * m0 * m0 * values0;
	vec2 temp1 = -6.0 * m1 * m1 * values1;

	vec3 mmm0 = m0 * m0 * m0;
	vec2 mmm1 = m1 * m1 * m1;

	float dx = temp0[0] * x0.x + temp0[1] * x1.x + temp0[2] * x2.x + temp1[0] * x3.x + temp1[1] * x4.x + mmm0[0] * p0.x + mmm0[1] * p1.x + mmm0[2] * p2.x + mmm1[0] * p3.x + mmm1[1] * p4.x;
	float dy = temp0[0] * x0.y + temp0[1] * x1.y + temp0[2] * x2.y + temp1[0] * x3.y + temp1[1] * x4.y + mmm0[0] * p0.y + mmm0[1] * p1.y + mmm0[2] * p2.y + mmm1[0] * p3.y + mmm1[1] * p4.y;
	float dz = temp0[0] * x0.z + temp0[1] * x1.z + temp0[2] * x2.z + temp1[0] * x3.z + temp1[1] * x4.z + mmm0[0] * p0.z + mmm0[1] * p1.z + mmm0[2] * p2.z + mmm1[0] * p3.z + mmm1[1] * p4.z;
	float dw = temp0[0] * x0.w + temp0[1] * x1.w + temp0[2] * x2.w + temp1[0] * x3.w + temp1[1] * x4.w + mmm0[0] * p0.w + mmm0[1] * p1.w + mmm0[2] * p2.w + mmm1[0] * p3.w + mmm1[1] * p4.w;

	return vec4(dx, dy, dz, dw) * 49.0;
}


// -------------------------------------------------------------------------------------
// Default _persistence should be 0.5
//
vec3 curlNoise( vec3 _p, float _time, int _octaves, float _persistence )
{
	vec4 xNoisePotentialDerivatives = vec4(0.0);
	vec4 yNoisePotentialDerivatives = vec4(0.0);
	vec4 zNoisePotentialDerivatives = vec4(0.0);
	
	float tmpPersistence = _persistence;
	
	for (int i = 0; i < _octaves; ++i)
	{
		float scale = (1.0 / 2.0) * pow(2.0, float(i));
		
		float noiseScale = pow(tmpPersistence, float(i));
		if (tmpPersistence == 0.0 && i == 0) //fix undefined behaviour
		{
			noiseScale = 1.0;
		}
		
		xNoisePotentialDerivatives += simplexNoiseDerivatives(vec4(  _p 								  * pow(2.0, float(i)), _time)) * noiseScale * scale;
		yNoisePotentialDerivatives += simplexNoiseDerivatives(vec4(( _p + vec3(123.4, 129845.6, -1239.1)) * pow(2.0, float(i)), _time)) * noiseScale * scale;
		zNoisePotentialDerivatives += simplexNoiseDerivatives(vec4(( _p + vec3(-9519.0, 9051.0, -123.0))  * pow(2.0, float(i)), _time)) * noiseScale * scale;
	}
	
	//compute curl
	vec3 curlVal = vec3( zNoisePotentialDerivatives[1] - yNoisePotentialDerivatives[2],
						 xNoisePotentialDerivatives[2] - zNoisePotentialDerivatives[0],
						 yNoisePotentialDerivatives[0] - xNoisePotentialDerivatives[1] );
	
	
	return curlVal;
}

uniform sampler2D u_particlePosAndAgeTexture;
uniform sampler2D u_particleVelTexture;

uniform float u_time;
uniform float u_timeStep;

uniform float u_particleMaxAge;

uniform float u_noisePositionScale = 1.5; // some start values in case we don't set any
uniform float u_noiseMagnitude = 0.075;
uniform float u_noiseTimeScale = 1.0 / 4000.0;
uniform float u_noisePersistence = 0.2;
uniform vec3 u_baseSpeed = vec3( 0.5, 0.0, 0.0 );

const int OCTAVES = 3;

layout (location = 0) out vec4 outputPosAge;
layout (location = 1) out vec4 outputVel;

in vec2 vTexCoord;
//////////////////////////////

// -----------------------------------------------------------
void main (void)
{
	vec2 texCoord = vTexCoord;
	
	vec4 posAndAge = texture2D( u_particlePosAndAgeTexture, texCoord );
	
	vec3 pos = posAndAge.xyz;
	float age = posAndAge.w;
	
	age += u_timeStep;
	
	if( age > u_particleMaxAge )
	{
		age = 0.0;
		
		float spawnRadius = 0.1;
		pos = randomPointOnSphere( vec3( rand( texCoord + pos.xy ), rand( texCoord.xy + pos.yz ), rand( texCoord.yx + pos.yz ))) * spawnRadius;
	}
	
	vec3 noisePosition = pos  * u_noisePositionScale;
	float noiseTime    = u_time * u_noiseTimeScale;
	
	vec3 noiseVelocity = curlNoise( noisePosition, noiseTime, OCTAVES, u_noisePersistence )  * u_noiseMagnitude;
	vec3 totalVelocity = u_baseSpeed + noiseVelocity;
	
	vec3 newPos = pos + totalVelocity * u_timeStep;
	vec3 vel = newPos - pos;
	
	pos = newPos;
	
	outputPosAge = vec4( pos, age );
	outputVel = vec4( vel, 1.0 );
	
}

