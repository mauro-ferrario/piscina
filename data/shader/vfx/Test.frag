#version 400
precision highp float;

uniform vec2 sun;

//#define swim ((time*.3+sin(time*.5+5.0)*.3)*3.0)

uniform sampler2DRect tex;
uniform sampler2DRect colorTex;
uniform sampler2DRect positionTex;
uniform sampler2DRect normalAndDepthTex;
uniform mat4 projectionMatrix;

uniform sampler2DRect chan0;
uniform sampler2DRect chan1;
uniform sampler2DRect chan2;
uniform sampler2DRect chan3;

in vec2 vTexCoord;
out vec4 outputColor;

//#define time (iTime+32.2)

uniform vec3 lightDir;
uniform float zDir;

uniform float sunRadius;
uniform float sunIntensity;
uniform float sunPowR;
uniform float sunPowG;
uniform float sunPowB;
uniform float sunInfluenceOnMesh;

uniform float ripMod1;
uniform float ripMod2;
uniform float ripMod3;
uniform float ripMod4;
uniform float ripMod5;
uniform float ripMod6;
uniform float ripMod7;
uniform float ripMod8;

uniform float beamMod1;
uniform float beamMod2;
uniform float beamMod3;
uniform float beamMod4;
uniform float beamMod5;
uniform float beamMod6;
uniform float beamMod7;
uniform float beamMod8;
uniform float beamMod9;
uniform float beamMod10;
uniform float beamMod11;
uniform float beamMod12;
uniform float beamMod13;
uniform float beamMod14;
uniform float beamMod15;
uniform float beamIntensity;

uniform float contrast;
uniform float saturation;
uniform float brightness;

uniform float lumMult;
uniform float lumMix;
uniform float fogMix;

uniform float waterMod1;
uniform float waterMod2;
uniform float waterMod3;
uniform float waterMod4;
uniform float waterMod5;
uniform float waterMod6;
uniform float waterMod7;
uniform float waterMod8;
uniform float waterMod9;
uniform float waterMod10;




#define csb(f, con, sat, bri) mix(vec3(.5), mix(vec3(dot(vec3(.2125, .7154, .0721), f*bri)), f*bri, sat), con)

/*
vec3 csb(vec3 f, float con, float sat, float bri) {
 	vec3 tt = mix(vec3(dot(vec3(.2125, .7154, .0721), f*bri)));
 	vec3 rr = mix(vec3(.5), tt, f*bri, sat), con);
 	return rr;
 }
*/

uniform float iTime;
uniform float nearClip;
uniform float farClip;

float swim() {return (iTime*.3+sin(iTime*.5+5.0)*.3)*3.0;}
float hash( float n ){return fract(sin(n)*43758.5453123);}
float Bubble(vec2 loc, vec2 pos, float size)
{
	vec2 v2 = loc-pos;
	float d = dot(v2, v2)/size;
	if (d > 1.0) return pow(max(0.0,1.5-d), 3.0) *5.0;
	d = pow(d, 6.0)*.85;
	
	// Top bright spot...
	v2 = loc-pos+vec2(-size*7.0, +size*7.0);
	d += .8 / max(sqrt((dot(v2, v2))/size*8.0), .3);
	// Back spot...
	v2 = loc-pos+vec2(+size*7.0, -size*7.0);
	d += .2 / max((dot(v2, v2)/size*4.0), .3);
	return d;
}

vec3 GetColour(vec4 p, vec3 n, vec3 org, vec3 dir)
{
	vec3 colour = texture(colorTex,vTexCoord).rgb;
	float lum = clamp(dot(n, lightDir), 0.0, 1.0);

	float v = clamp(-(n.y-.1)*6.2, 0.3, 1.0);
	v+=lumMult;
 	colour = mix(colour, vec3(v*.8, v*.9, v*1.0) * lum,lumMix);
	colour *= vec3(v*.8, v*.9, v*1.0);

	colour += vec3(0.0, .01,.13) * abs(n.y);


	vec2 wat = vec2(p.x*waterMod1, p.z*waterMod2);//*5.3;

	wat +=  (texture(chan0, (wat*waterMod3+iTime*waterMod4)*waterMod5).z -
			 texture(chan1, mod(wat*waterMod6-iTime*waterMod7,1024)).y) * waterMod8;

	float	i = texture(chan0, vec2(mod(wat* .5+iTime,1024).x, mod(wat* .5+iTime,1024).y)).x;
	i = min(pow(max(0.0, i-.2), 1.0) * 1.0, .6)*.968;
	colour += vec3(i*.5, i, i)*max(n.y, 0.0);

	
	// float shad = 1;//Shadow(p.xyz, lightDir);
	// colour = mix(vec3(0.1),colour, min(shad+.4, 1.0));

//	float dis = length(org,p.xyz);
	float fogAmount;// = clamp(max((dis-.5),0.0)*.0003, 0.0, 0.870);
	

	colour = mix(colour, vec3(.05, .31, .49), fogMix );

	vec3 col;
	vec2 iResolution = vec2(2560,1440);
	vec2 uv = (vTexCoord.xy / iResolution.xy) - vec2(.5);
//	vec2 uv = (vTexCoord.xy);
	uv.x*=iResolution.x/iResolution.y;
	i = max(0.0, sunRadius-length(sun-uv.xy));
	//col.rgb = vec3(pow(i, sunPowR), pow(i, sunPowG), pow(i, sunPowB)) * sunIntensity;
	//colour.rgb *= col*lum*sunInfluenceOnMesh;
	//fogAmount =  clamp(distance(org.z, p.z)/(farClip - nearClip), 1.0,0.0);

	return colour;

	
}

void main() {
	float time = iTime;
	vec2 iResolution = vec2(2560,1440);
	vec2 uv = (vTexCoord.xy / iResolution.xy) - vec2(.5);
//	vec2 uv = (vTexCoord.xy);
	uv.x*=iResolution.x/iResolution.y;

	vec2 iResolution2 = vec2(1024,1024);
	vec2 uv2 = (vTexCoord.xy / iResolution2.xy) - vec2(.5);
//	vec2 uv = (vTexCoord.xy);
	uv2.x*=iResolution2.x/iResolution2.y;

	 // vec3 pos = vec3(1.3, sin(time+4.3)*.18-.05, sin(-time*.15)*5.0-1.35);
	vec3 pos = vec3(0,0,0);


	vec3 dir = normalize(vec3(uv, zDir));

	vec4 col;

	// sun 

	float i = max(0.0, sunRadius-length(sun-uv));
	col.rgb = vec3(pow(i, sunPowR), pow(i, sunPowG), pow(i, sunPowB)) * sunIntensity;
	
	// water fx
	col.rgb = mix(col.rgb, vec3(0.0, .25, .45), ((uv.y)*.65) * 1.8);

	// Add water ripples...
	if (uv.y <= 0.0) {
		float d = (ripMod1-pos.y) / -uv.y;
		vec2 wat = (dir * d).xz-pos.xz;
		wat +=  (texture(chan2, (wat*ripMod2+iTime*ripMod3)*ripMod4).z - texture(chan3, wat*ripMod5-iTime*ripMod6).y) * ripMod7;

	//	 wat +=  (texture(chan2, (wat*40*iTime)).z - texture(chan3, wat*40*-iTime).y) * .4;
		
		i = texture(chan3, mod(wat* ripMod8+iTime,1024)).x;
		col += vec4(vec3(i) * max(abs(uv.y), 0.0),1);
	}

	// mesh lighting
	vec3 doLightMesh = texture(normalAndDepthTex,vTexCoord).rgb;
	if(doLightMesh != vec3(0,0,0)) {
		vec4 loc = texture(positionTex,vTexCoord);
		col.rgb = doLightMesh;
		col.rgb = GetColour(vec4(loc.rgb+pos.rgb, 1.0), doLightMesh, pos, dir);
	}

	
	//Light beams...
	vec2 beam = dir.xy;	
	beam.x *= (-beam.y-beamMod1)*beamMod2;
	float bright = 
				- sin(beam.y * beamMod3 + beam.x * beamMod4 + iTime *beamMod5) *beamMod6 
				- sin(beam.y + beam.x * beamMod7 + iTime *beamMod8) *beamMod9
				- cos(              + beam.x * beamMod10 - iTime *beamMod11) *beamMod12 
				- sin(              - beam.x * beamMod13 + iTime * beamMod14) * beamMod15;
	bright *= max(0.0, texture(chan1, (uv*.07-swim()*.04)).y);
	col.rgb += vec3(clamp(bright,0.0,1.0)) *beamIntensity;

	// Bubbles...
	// for (float i = 0.0; i < 50.0; i+=1.0)
	// {
	// 	float t = time+1.27;
	// 	float f = floor((t+2.0) / 4.0);
	// 	vec2 pos = vec2(.4, -.9) + vec2(0.0, mod(t+(i/50.0)+hash(i+f)*.7, 4.0));
	// 	pos.x += hash(i)*.7 * (uv.y+.6);
		
	// 	pos += texture(chan3, (uv*.3-time*.1+(i/80.0))).z * .05;
	// 	float d = Bubble(pos, vec2(uv.x,1-uv.y), .002*hash(i-f)+.00015);
	// 	d *= hash(i+f+399.0) *.3+.08;
	// 	col.rgb = mix(col.rgb, vec3(.6+hash(f*323.1+i)*.4, 1.0, 1.0), d);
	// }

	// Contrast, saturation and brightness...
	col.rgb = csb(col.rgb, contrast, saturation, brightness);
	
	// Vignette...
	uv = ((vTexCoord.xy / iResolution.xy) * 2) - 1.0;
	col.rgb = mix(col.rgb,vec3(.0), abs(uv.x*0.695)*abs(uv.y*0.695));

	// Fade in...
	//col.rgb *= smoothstep( 0.0, 2.5, iTime );

	outputColor = vec4(col.rgb,1);
}
