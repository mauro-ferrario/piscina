#pragma once
#include "ofMain.h"
#include "Processor.hpp"
#include "ofxGui.h"

namespace ofxDeferred {
	class UnderwaterPass : public RenderPass {
	protected:
		ofShader shader;
		glm::mat4 projection;
		float radius = 2.0;
		float darkness = 0.8;

		

	public:
		ofImage chan0;
		ofImage chan1;
		ofImage chan2;
		ofImage chan3;

		using Ptr = shared_ptr<UnderwaterPass>;
		UnderwaterPass(const ofVec2f& size);
		void render(ofFbo& readFbo, ofFbo& writeFbo, GBuffer& gbuffer);
		void update(const ofCamera& cam);
		//void setOcculusionRadius(float radius) { this->radius = radius; }
		//void setDarkness(float darkness) { this->darkness = darkness; }

		float nearClip;
		float farClip;
		bool isShaderDirty;
	
		void createGui();
		ofxPanel sunGui;
		ofParameter<ofVec2f> sunPos;
		ofParameter<float> sunRadius;
		ofParameter<float> sunIntensity;
		ofParameter<float> sunPowR;
		ofParameter<float> sunPowG;
		ofParameter<float> sunPowB;
		ofParameter<float> sunInfluenceOnMesh;

		ofxPanel rayGui;
		ofParameter<ofVec3f> lightDir;
		ofParameter<float> zDir;

		ofxPanel ripplesGui;
		ofParameter<float> mod1;
		ofParameter<float> mod2;
		ofParameter<float> mod3;
		ofParameter<float> mod4;
		ofParameter<float> mod5;
		ofParameter<float> mod6;
		ofParameter<float> mod7;
		ofParameter<float> mod8;

		ofxPanel beamsGui;
		ofParameter<float> beamsMod1;
		ofParameter<float> beamsMod2;
		ofParameter<float> beamsMod3;
		ofParameter<float> beamsMod4;
		ofParameter<float> beamsMod5;
		ofParameter<float> beamsMod6;
		ofParameter<float> beamsMod7;
		ofParameter<float> beamsMod8;
		ofParameter<float> beamsMod9;
		ofParameter<float> beamsMod10;
		ofParameter<float> beamsMod11;
		ofParameter<float> beamsMod12;
		ofParameter<float> beamsMod13;
		ofParameter<float> beamsMod14;
		ofParameter<float> beamsMod15;
		ofParameter<float> beamIntensity;

		ofxPanel csbGui;
		ofParameter<float> contrast;
		ofParameter<float> saturation;
		ofParameter<float> brightness;

		ofxPanel renderGui;
		ofParameter<float> lumMult;
		ofParameter<float> lumMix;
		ofParameter<float> waterMod1;
		ofParameter<float> waterMod2;
		ofParameter<float> waterMod3;
		ofParameter<float> waterMod4;
		ofParameter<float> waterMod5;
		ofParameter<float> waterMod6;
		ofParameter<float> waterMod7;
		ofParameter<float> waterMod8;
		ofParameter<float> waterMod9;
		ofParameter<float> waterMod10;
		ofParameter<float> mixFog;
		void guiDraw();
	};
}
