#include "UnderwaterPass.hpp"

using namespace ofxDeferred;

UnderwaterPass::UnderwaterPass(const ofVec2f& size) : RenderPass(size, "UnderwaterPass") {
	shader.load("shader/vfx/PassThru.vert", "shader/vfx/Test.frag");

	ofImage tmpImg;
	chan0.load("waterPass/0.jpg");
	chan1.load("waterPass/1.jpg");
	chan2.load("waterPass/2.jpg");
	chan3.load("waterPass/3.jpg");

	chan0.getTextureReference().setTextureWrap(GL_REPEAT, GL_REPEAT);
	chan1.getTextureReference().setTextureWrap(GL_REPEAT, GL_REPEAT);
	chan2.getTextureReference().setTextureWrap(GL_REPEAT, GL_REPEAT);
	chan3.getTextureReference().setTextureWrap(GL_REPEAT, GL_REPEAT);

	createGui();
}

void UnderwaterPass::update(const ofCamera& cam) {
	projection = cam.getProjectionMatrix();
	nearClip = cam.getNearClip();
	farClip = cam.getFarClip();

	if (isShaderDirty) {
		ofLogNotice() << "Reloading Shader.";
		shader.load("shader/vfx/PassThru.vert", "shader/vfx/Test.frag");
		GLint err = glGetError();
		if (err != GL_NO_ERROR) {
			ofLogNotice() << "Load Shader came back with GL error:	" << err;
		}
		isShaderDirty = false;
	}
}

void UnderwaterPass::render(ofFbo &readFbo, ofFbo &writeFbo, GBuffer &gbuffer) {

	writeFbo.begin();
	ofPushStyle();
	ofClear(0);

	//ofEnableNormalizedTexCoords();
	shader.begin();
	shader.setUniformMatrix4f("projectionMatrix", projection);
	shader.setUniformTexture("colorTex", gbuffer.getTexture(GBuffer::TYPE_ALBEDO), 1);
	shader.setUniformTexture("positionTex", gbuffer.getTexture(GBuffer::TYPE_POSITION), 2);
	shader.setUniformTexture("normalAndDepthTex", gbuffer.getTexture(GBuffer::TYPE_DEPTH_NORMAL), 3);
	shader.setUniformTexture("chan0", chan0.getTexture(), 4);
	shader.setUniformTexture("chan1", chan1.getTexture(), 5);
	shader.setUniformTexture("chan2", chan2.getTexture(), 6);
	shader.setUniformTexture("chan3", chan3.getTexture(), 7);
	shader.setUniform1f("iTime", ofGetElapsedTimef());
	shader.setUniform1f("nearClip", nearClip);
	shader.setUniform1f("farClip", farClip);
	shader.setUniform2f("sun", sunPos.get().x, sunPos.get().y);
	shader.setUniform1f("sunRadius", sunRadius);
	shader.setUniform1f("sunIntensity", sunIntensity);
	shader.setUniform1f("sunPowR", sunPowR);
	shader.setUniform1f("sunPowG", sunPowG);
	shader.setUniform1f("sunPowB", sunPowB);
	shader.setUniform1f("sunInfluenceOnMesh", sunInfluenceOnMesh);
	shader.setUniform3f("lightDir", lightDir.get().x, lightDir.get().y, lightDir.get().z);
	shader.setUniform1f("zDir", zDir);
	shader.setUniform1f("ripMod1", mod1);
	shader.setUniform1f("ripMod2", mod2);
	shader.setUniform1f("ripMod3", mod3);
	shader.setUniform1f("ripMod4", mod4);
	shader.setUniform1f("ripMod5", mod5);
	shader.setUniform1f("ripMod6", mod6);
	shader.setUniform1f("ripMod7", mod7);
	shader.setUniform1f("ripMod8", mod8);
	shader.setUniform1f("beamMod1", beamsMod1);
	shader.setUniform1f("beamMod2", beamsMod2);
	shader.setUniform1f("beamMod3", beamsMod3);
	shader.setUniform1f("beamMod4", beamsMod4);
	shader.setUniform1f("beamMod5", beamsMod5);
	shader.setUniform1f("beamMod6", beamsMod6);
	shader.setUniform1f("beamMod7", beamsMod7);
	shader.setUniform1f("beamMod8", beamsMod8);
	shader.setUniform1f("beamMod9", beamsMod9);
	shader.setUniform1f("beamMod10", beamsMod10);
	shader.setUniform1f("beamMod11", beamsMod11);
	shader.setUniform1f("beamMod12", beamsMod12);
	shader.setUniform1f("beamMod13", beamsMod13);
	shader.setUniform1f("beamMod14", beamsMod14);
	shader.setUniform1f("beamMod15", beamsMod15);
	shader.setUniform1f("beamIntensity", beamIntensity);
	shader.setUniform1f("contrast", contrast);
	shader.setUniform1f("saturation", saturation);
	shader.setUniform1f("brightness", brightness);
	shader.setUniform1f("waterMod1", waterMod1);
	shader.setUniform1f("waterMod2", waterMod2);
	shader.setUniform1f("waterMod3", waterMod3);
	shader.setUniform1f("waterMod4", waterMod4);
	shader.setUniform1f("waterMod5", waterMod5);
	shader.setUniform1f("waterMod6", waterMod6);
	shader.setUniform1f("waterMod7", waterMod7);
	shader.setUniform1f("waterMod8", waterMod8);
	shader.setUniform1f("waterMod9", waterMod9);
	shader.setUniform1f("waterMod10", waterMod10);
	shader.setUniform1f("lumMult", lumMult);
	shader.setUniform1f("lumMix", lumMix);
	shader.setUniform1f("fogMix", mixFog);

	readFbo.draw(0, 0);
	shader.end();
	ofDisableNormalizedTexCoords();

	ofPopStyle();
	writeFbo.end();
}

void UnderwaterPass::createGui() {
	sunGui.setup("SUN","XML/sun.xml");
	sunGui.add(sunPos.set("POSITION", ofVec2f(-.31, -.87), ofVec2f(-1, -1), ofVec2f(1, 1)));
	sunGui.add(sunRadius.set("RADIUS", 1.31, .1, 2.));
	sunGui.add(sunIntensity.set("INTENSITY", 0.786, 0, 1));
	sunGui.add(sunPowR.set("POW R", 1.9, -10, 10));
	sunGui.add(sunPowG.set("POW G", 1., -10, 10));
	sunGui.add(sunPowB.set("POW B", .8, -10, 10));
	sunGui.add(sunInfluenceOnMesh.set("INFLUENCE ON MESH", 2.0, 1.0, 20.0));

	rayGui.setup("RAYS", "XML/ray.xml");
	rayGui.add(lightDir.set("LIGHT DIR", ofVec3f(-.915, .4, .93), ofVec3f(-2, -2, -2), ofVec3f(2, 2, 2)));
	rayGui.add(zDir.set("Z DIR", -1.4, -10, 10));

	ripplesGui.setup("WAVES", "XML/waves.xml");
	ripplesGui.add(mod1.set("MOD 1", -2.1, -20, 20));
	ripplesGui.add(mod2.set("MOD 2", .03, -2, 2));
	ripplesGui.add(mod3.set("MOD 3", .01, -2, 2));
	ripplesGui.add(mod4.set("MOD 4", .1, -2, 2));
	ripplesGui.add(mod5.set("MOD 5", .02, -2, 2));
	ripplesGui.add(mod6.set("MOD 6", .01, -2, 2));
	ripplesGui.add(mod7.set("MOD 7", .4, -2, 2));
	ripplesGui.add(mod8.set("MOD 8", 15, -100, 100));

	beamsGui.setup("LIGHTBEAMS", "XML/beams.xml");
	beamsGui.add(beamsMod1.set("MOD 1", -.6, -2, 2));
	beamsGui.add(beamsMod2.set("MOD 2", .8, -2, 2));
	beamsGui.add(beamsMod3.set("MOD 3", 12.0, -100, 100));
	beamsGui.add(beamsMod4.set("MOD 4", 13.0, -100, 100));
	beamsGui.add(beamsMod5.set("MOD 5", .530, -2, 2));
	beamsGui.add(beamsMod6.set("MOD 6", .1, -2, 2));
	beamsGui.add(beamsMod7.set("MOD 7", 17.0, -100, 100));
	beamsGui.add(beamsMod8.set("MOD 8", .60, -2, 2));
	beamsGui.add(beamsMod9.set("MOD 9", .1, -2, 2));
	beamsGui.add(beamsMod10.set("MOD 10", 13.0, -100, 100));
	beamsGui.add(beamsMod11.set("MOD 11", .40, -2, 2));
	beamsGui.add(beamsMod12.set("MOD 12", .1, -2, 2));
	beamsGui.add(beamsMod13.set("MOD 13", 52.23, -100, 100));
	beamsGui.add(beamsMod14.set("MOD 14", 1.8, -2, 2));
	beamsGui.add(beamsMod15.set("MOD 15", .1, -2, 2));
	beamsGui.add(beamIntensity.set("INTENSITY", 1.6, -5, 5));

	csbGui.setup("C/S/B", "XML/csb.xml");
	csbGui.add(contrast.set("CONTRAST", 1.1, -2, 2));
	csbGui.add(saturation.set("SATURATION", 1.05, -2, 2));
	csbGui.add(brightness.set("BRIGHTNESS", 1.22, -2, 2));

	renderGui.setup("RENDERER", "XML/render.xml");
	renderGui.add(lumMult.set("LUMINANCE MULTI", .15, -5, 5));
	renderGui.add(lumMix.set("LUMINANCE MIX", .35, 0, 1));
	renderGui.add(mixFog.set("FOG MIX", .6, 0, 1));
	renderGui.add(waterMod1.set("WATERMOD 1", .63, -1, 1));
	renderGui.add(waterMod2.set("WATERMOD 2", .2, -1, 1));
	renderGui.add(waterMod3.set("WATERMOD 3", .0015, -1, 1));
	renderGui.add(waterMod4.set("WATERMOD 4", .00004, -1, 1));
	renderGui.add(waterMod5.set("WATERMOD 5", .1, -1, 1));
	renderGui.add(waterMod6.set("WATERMOD 6", .13, -1, 1));
	renderGui.add(waterMod7.set("WATERMOD 7", .03, -1, 1));
	renderGui.add(waterMod8.set("WATERMOD 8", 12.4, -50, 50));
	renderGui.add(waterMod9.set("WATERMOD 9", .3, -1, 1));
	renderGui.add(waterMod10.set("WATERMOD 10", .03, -1, 1));

	sunGui.setPosition(ofGetWidth()-sunGui.getWidth(),sunGui.getPosition().y);
	rayGui.setPosition(sunGui.getPosition().x, sunGui.getPosition().y + sunGui.getHeight());
	ripplesGui.setPosition(sunGui.getPosition().x, rayGui.getPosition().y + rayGui.getHeight());
	beamsGui.setPosition(sunGui.getPosition().x, ripplesGui.getPosition().y + ripplesGui.getHeight());
	csbGui.setPosition(sunGui.getPosition().x, beamsGui.getPosition().y + beamsGui.getHeight());
	renderGui.setPosition(sunGui.getPosition().x-sunGui.getWidth(), sunGui.getPosition().y);
}

void UnderwaterPass::guiDraw() {
	sunGui.draw();
	rayGui.draw();
	ripplesGui.draw();
	beamsGui.draw();
	csbGui.draw();
	renderGui.draw();
}
