#pragma once

#include "Processor.hpp"
#include "PointLightPass.hpp"
#include "SsaoPass.hpp"
#include "ShadowLightPass.hpp"
#include "BloomPass.hpp"
#include "DofPass.hpp"
#include "EdgePass.hpp"
#include "Blur.h"
#include "BgPass.hpp"
#include "UnderwaterPass.hpp"

using ofxDeferredProcessing = ofxDeferred::Processor;
